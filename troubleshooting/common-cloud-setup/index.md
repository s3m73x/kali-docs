---
title: Common Cloud Based Setup Information
description:
icon:
date: 2020-9-18
type: post
weight: 11
author: ["gamb1t",]
tags: ["",]
keywords: ["",]
og_description:
---

A cloud image is designed to be lightweight while having the core tools to get the job done. If the base install is lacking something that may be needed, then the following should cover most questions.

- [Metapackages](https://www.kali.org/docs/general-use/metapackages/) are helpful to quickly and easily install many tools at once. This is good to easily get access to specialized tools, such as information gathering tools.
- [Kali Network Repositories](https://www.kali.org/docs/general-use/kali-linux-sources-list-repositories/) and [Kali Branches](https://www.kali.org/docs/introduction/kali-branches/) are useful to know about in case a user may want to have a more static install or not.

Due to the EOL of Python 2, there may be some confusion on certain topics involving it.

- What happened to _blank_ [Python 2 tool](https://www.kali.org/news/python-2-end-of-life/)?
- How to get [Python 2 working](https://www.kali.org/docs/general-use/using-eol-python-versions/) after it has been removed.

### Kali in the cloud
- [AWS Information](https://www.kali.org/docs/cloud/aws/)
- [Getting NVIDIA cards working](https://www.kali.org/docs/general-use/install-nvidia-drivers-on-kali-linux/)