---
title: NetHunter USB-Arsenal
description:
icon:
date: 2019-11-29
type: post
weight: 210
author: ["re4son",]
tags: ["",]
keywords: ["",]
og_description:
---

USB-Arsenal is the control centre for USB based attacks.
It is used to enable USB gadget modes using the *USB Function Selector*:

&nbsp;

![](./nethunter-usbarsenal1.jpg)

&nbsp;

If mass storage gadget mode has been enabled then .iso and .img files can be mounted in the *Image Mounter* menu, before connecting the device to the USB port of a computer which will then treat NetHunter as a USB drive on which the image was installed:

&nbsp;

![](./nethunter-usbarsenal2.jpg)

&nbsp;

If RNDIS gadget mode is enabled then the *USB Network Tethering* menu can be used for various network interface based attacks:

&nbsp;

![](./nethunter-usbarsenal3.jpg)